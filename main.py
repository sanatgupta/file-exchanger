File1 = input("Input file 1 path:") # Path for 1st File
File2 = input("Input file 2 path:") # Path for 2nd File

Input = open(File1,'r+')
Output = open(File2,'r+')

content_Input = Input.readlines()
content_Output = Output.readlines()


Input.seek(0) 
Input.truncate() 


Output.seek(0) 
Output.truncate() 

Input.writelines(content_Output)
Output.writelines(content_Input)
Input.close()
Output.close()
